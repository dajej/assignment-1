<?php
include_once("IModel.php");
include_once("Book.php");
include_once(__DIR__ . "/../view/ErrorView.php")

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
   * @throws PDOException
     */
    public function __construct($db = null)
    {
      if ($db)
    {
      $this->db = $db;
    }
    else
    {
      try {
      $this->db = new PDO('mysql:host=localhost;dbname=oblig 1;charset=utf8','root', '',

      array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)  );
    }
      catch (PDOException $e)
      {
        $view = new ErrorView('Connection failed: ' . $e->getMessage());
        $view->create();
      }

    }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
   * @throws PDOException
     */
    public function getBookList(){
    $res = array();
    try {
      $stmt = $this->db->query('SELECT * FROM book ORDER BY ID'  );
        while($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
          $res[] = new Book($row['title'], $row['author'], $row['description'], $row['ID']);
        }
      } catch (PDOException $e)
      {
        $view = new ErrorView('Failed retrieving books: ' . $e->getMessage());
        $view->create();
      }
    return $res;
  }
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
   * @throws PDOException
     */
    public function getBookById($id)
    {
    $res = null;
    try {

      $stmt = $this->db->query("SELECT ID, title, author, description FROM book WHERE ID=$id");
      $row = $stmt->fetch((PDO::FETCH_ASSOC));
      } catch (PDOException $e)
      {
       $view = new ErrorView('Failed retrieving book: ' . $e->getMessage());
       $view->create();
      }
      if ($row['ID'] > 0 && !(is_int($row['ID']))) {
       $res = new Book($row['title'], $row['author'],  $row['description'], $id);
      }
      return $res;
    }



    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
   * @throws PDOException
     */
    public function addBook($book)
    {
      try {
      $stmt = $this->db->prepare("INSERT INTO book(title, author, description) VALUES(:title, :author, :description)");
      $stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
      $stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
      $stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
      $book->id = 1;
      if(empty($book->title) || empty($book->author)){
         $view = new errorView("fail title/author ");
         $view->create();
       }
      else {$book = $stmt->execute();}
    } catch (PDOException $e) {
      $view = new errorView("fail title/author " . $e->getMessage());
      $view->create();
      }
    }


    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      try {
        $stmt = $this->db->prepare("UPDATE book SET title=:title, author=:author, description=:description WHERE id=$book->id");
        $stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
        $stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
        $stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
        if(empty($book->title) || empty($book->author)){
          $view = new errorView("fail title/author ");
          $view->create();
        }
        else {$book = $stmt->execute();}
    } catch (Exception $e) {
      $view = new errorView("fail title/author " . $e->getMessage());
      $view->create();
    }
}
    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
     try {
      $delete = $this->db->prepare("DELETE FROM book WHERE ID=$id");
      $delete->execute();
    } catch (Exception $e) {
    $view = new ErrorView("Caught exception: ". $e->getMessage());
    $view->create();
    }
  }
}
?>
